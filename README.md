## How to get flatpaks show in dmenu
## check if you have **/home/*user*/.local/bin** in you $PATH
```sh
$PATH
```
you we get something like this 
```sh
bash: **/home/user/.local/bin** :/usr/local/bin :/usr/bin :/bin :/usr/local/games
```
## now create a file with the "programname"
```sh
vim .local/bin/*program*
```
Or
```sh
gedit .local/bin/*program*
```
## now just type flatpak run and the program name or program id whatever its called it is given at the bottom of the flathub page of the program
eg:
```sh
 flatpak run prog.ram.name
```
you can just get the program id by using the following command
```sh
flatpak list
```
output:
```sh
Name          Application ID    Version ...
program       prog.ram.name     0.0
program2       prog.ram.name2   0.0
program3       prog.ram.name3   0.0
```
